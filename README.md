etcd
=========

Install and configure etcd key-value store.<br> 
You can install an entire cluster or a single node (Based on provided variable `etcd_config`)

Requirements
------------
Collections:
- community.general
- community.crypto

Role Variables
--------------

The default values for the variables are set in `defaults/main.yml`:
```yml
# defaults file for etcd
etcd_version: "v3.4.23" #Version that will be downloaded and installed
etcd_download_url: https://github.com/etcd-io/etcd/releases/download/{{ etcd_version }}/etcd-{{ etcd_version }}-linux-amd64.tar.gz

etcd_protocol: http # Can be used in etcd_config variables. https can be used only with 'client/transport security' options and certificates 
etcd_client_port: 2379 # Default port for client url
etcd_peer_port: 2380 # Default port for cluster peer url

etcd_user: "etcd"
etcd_group: "etcd"
etcd_conf_dir: "/etc/etcd" # Default configuration directory
#etcd_root_dir: "/opt/etcd"  # Optional: Root directory that can be used or overwritten in etcd_config variable for subdirs data-dir and wal-dir
etcd_config: {} # Generates the /etc/etcd/etcd.conf file. You can provide any etcd configuration option. (See available options in documentation )
```

**TLS Encryption for Client and Cluster communication:**

You could use TLS encryption for client and cluster communication.<br>
Provide the source and destination paths to your certificate files and Ansible will copy them on the remote machines or use certificates that already exist on the remote host.<br>
Make sure to include subject alternate names within your certificate.<br>
Etcd requires the `extended_key_usage` `TLS Web Client Authentication` and `TLS Web Server Authentication` in the certificate file.
```yml
etcd_protocol: https 
copy_etcd_member_cert: "{{ inventory_dir }}/group_vars/secrets/etcd.example.com.crt" 
copy_etcd_member_cert_key: "{{ inventory_dir }}/group_vars/secrets/etcd.example.com.key" 
copy_etcd_member_cert_dest: "/etc/ssl/certs/{{ copy_etcd_member_cert | basename }}"
copy_etcd_member_cert_key_dest: "/etc/ssl/private/{{ copy_etcd_member_cert_key | basename }}" 

# Or use path to existing cert on remote host 
existing_etcd_member_cert: "/etc/ssl/private/cert.crt" 
existing_etcd_member_cert_key: "/etc/ssl/private/cert.key"
```

Reference your certificates in the `etcd_config` variable:
```yml
etcd_config:
...
...
  client-transport-security:
    trusted-ca-file: "/etc/ssl/certs/ca-certificates.crt"
    cert-file: "{{ copy_etcd_member_cert_dest }}"
    key-file: "{{ copy_etcd_member_cert_key_dest }}"
  peer-transport-security:
    trusted-ca-file: "/etc/ssl/certs/ca-certificates.crt"
    cert-file: "{{ copy_etcd_member_cert_dest }}"
    key-file: "{{ copy_etcd_member_cert_key_dest }}"
```

Dependencies
------------

- No Dependencies


Example Inventory
----------------
```yml
all:
  hosts:
  children:
    postgres_cluster:
      hosts:
        db-patroni-01:
          ansible_host: 10.199.34.121
        db-patroni-02:
          ansible_host: 10.199.34.122
        db-patroni-03:
          ansible_host: 10.199.34.123 
```
Example Playbook
----------------
```yml
# Install etcd cluster with 3 nodes and use TLS encryption
- hosts: postgres_cluster
  become: yes
  gather_facts: yes
  roles:
    - role: etcd
      etcd_version: "v3.4.23" 
      etcd_download_url: https://files.repository.mueller.de/pgcluster/etcd-{{ etcd_version }}-linux-amd64.tar.gz
      etcd_protocol: https 
      copy_etcd_member_cert: "{{ inventory_dir }}/group_vars/secrets/db-postgres.example.com.crt" 
      copy_etcd_member_cert_key: "{{ inventory_dir }}/group_vars/secrets/db-postgres.example.com.key" 
      copy_etcd_member_cert_dest: "/etc/ssl/certs/{{ copy_etcd_member_cert | basename }}"
      copy_etcd_member_cert_key_dest: "/etc/ssl/private/{{ copy_etcd_member_cert_key | basename }}"
      etcd_root_dir: "/opt/etcd" 
      etcd_config:
        name: "{{ ansible_fqdn }}" # node name identifier
        data-dir: "/opt/etcd/data" # etcd data dir
        wal-dir: "/opt/etcd/wal"   # wal backup dir
        listen-client-urls: "{{ etcd_protocol }}://{{ ansible_default_ipv4.address }}:{{ etcd_client_port }},http://127.0.0.1:{{ etcd_client_port }}" # Listen address for client requests
        advertise-client-urls: "http://127.0.0.1:{{ etcd_client_port }}" # URL for client requests
        listen-peer-urls: "{{ etcd_protocol }}://{{ ansible_default_ipv4.address }}:{{ etcd_peer_port }}" # Listen address for cluster peers
        initial-advertise-peer-urls: "{{ etcd_protocol }}://{{ ansible_default_ipv4.address }}:{{ etcd_peer_port }}" # # URL for cluster peers
        initial-cluster-token: "etcd-{{ patroni_cluster_name }}"  # Cluster secret token
        initial-cluster-state: "new" # Initialize new cluster
        initial-cluster: "db-postgres-01.example.com={{ etcd_protocol }}://10.199.42.36:{{ etcd_peer_port }},db-postgres.example.com={{ etcd_protocol }}://10.199.42.37:{{ etcd_peer_port }},db-postgres-03.example.com={{ etcd_protocol }}://10.199.42.38:{{ etcd_peer_port }}"
        enable-grpc-gateway: true # Required for newer APIv3
        client-transport-security:
          trusted-ca-file: "/etc/ssl/certs/ca-certificates.crt"
          cert-file: "{{ copy_etcd_member_cert_dest }}"
          key-file: "{{ copy_etcd_member_cert_key_dest }}"
        peer-transport-security:
          trusted-ca-file: "/etc/ssl/certs/ca-certificates.crt"
          cert-file: "{{ copy_etcd_member_cert_dest }}"
          key-file: "{{ copy_etcd_member_cert_key_dest }}"
        #You can specify all hosts and IPs manually or loop through your inventory group (i.e. postgres_cluster) and grab hostname and ip automatically 
        #initial-cluster: "{% for host in groups['postgres_cluster'] %}{{ hostvars[host]['inventory_hostname'] }}={{ etcd_protocol }}://{{ hostvars[host]['ansible_host'] }}:{{ etcd_peer_port }}{% if not loop.last %},{% endif %}{% endfor %}"
      
```

**Important:**
- **Change variable `etcd_config.initial-cluster-state` from `new` to `existing` after the etcd cluster was deployd**

The cluster can also be updated to a newer version. Its recommended to set `serial:` at execution.
```yml
- hosts: postgresql
  become: yes
  gather_facts: yes
  serial: 1
  roles:
    - role: etcd
```

License
-------

GPLv3

Author Information
------------------

Oleg Franko
